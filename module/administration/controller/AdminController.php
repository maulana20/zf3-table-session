<?php
namespace Administration\Controller;

use Application\Controller\ParentController;
use Administration\Model\User;
use Administration\Model\Group;

class AdminController extends ParentController
{
	public function indexAction()
	{
		$this->loginAction();
	}
	
	public function loginAction()
	{
		$user = new User();
		$group = new Group();
		$request = $this->getRequest();
		
		if ((!empty($this->session->temp_username)) or (!empty($this->session->temp_password))) {
			$post = array(
				'user' => $this->session->temp_username,
				'password' => $this->session->temp_password,
			);
			unset($this->session->temp_userneme);
			unset($this->session->temp_password);
		} else {
			$post = array(
				'user' => $request->getPost('user'),
				'password' => $request->getPost('password'),
			);
		}
		
		$user_id = $user->getId($post['user']);
		
		if ( $user->isUserPassword($post['user'], $post['password']) ) {
			if ($user->isOnLogin($user_id)) {
				// $userLog->add($user_id, $post['user'].' login inuse.');
				$this->session->user_id = $user_id;
				$this->printResponse('inuse', 'Your login name is inuse !!!', array('flag' => 'alert', 'alert' => 'nama login terpakai !!!'));
			} else {
				$temp = NULL;
				$row = $user->getRow($user_id);
				$group_id = $row['group_id'];
				$this->session->user_id = $user_id;
				$this->session->user_name = $row['user_name'];
				$this->session->group_id = $group_id;
				
				$this->session->baseurl = $request->getPost('baseurl');
				$access = $group->getAccess($group_id);
				$access = unserialize($access);
				$this->setRole($access);
				$user->update($user_id, array('user_login' => time(), 'login_attempt' => 0,));
				$user->updateLifeTime($this->session->user_id, time()+ EXPIRED);
				
				$this->printResponse('success', 'login success', ['TESTSESSION' => $this->session->__getCode()]);
			}
		} else {
			if (!empty($user_id)) {
				// $userLog->add($user_id, 'Try Log in wrong password');
				$user->incPasswordAttempt($user_id);
			}
			
			$this->printResponse('failed', 'Username or Password not match', array('flag' => 'alert', 'alert' => 'username atau password salah'));
		}
	}
	
	public function noaccessAction()
	{
		$this->printResponse('failed', 'SESSION TIMEOUT', array('flag'=>'alert', 'alert'=>'sesi habis'));
	}
	
	public function nopopupAction()
	{
		$this->printResponse('failed', 'SESSION TIMEOUT', array('flag'=>'alert', 'alert'=>'sesi habis'));
	}
	
	public function noactionAction()
	{
		$this->printResponse('failed', 'THIS PAGE UNDER CONSTRUCTION !!!', array('flag'=>'alert', 'alert'=>'halaman sedang dibuat'));
	}
	
	public function logoutAction()
	{
		$user = new User();
		if (!empty($this->session->user_id)) $user->updateLifeTime($this->session->user_id, time());
		$this->destroyRole();
		
		$this->printResponse('success', 'Anda telah logout !', null);
	}
	
	public function isonloginAction()
	{
		$user = new User();
		
		$response = ['status' => 'failed', 'message' => 'Gagal Login'];
		if($this->session->user_name) $response = ['status' => 'success', 'message' => 'Berhasil login'];
		
		$this->__saveSession();
		
		echo json_encode($response);
		exit();
	}
}
