<?php
namespace Administration\Model;

use Application\Model\Table_Gateway_Adapter;

class User extends Table_Gateway_Adapter
{
	public function update($id, $data)
	{
		$this->init('tblUser')->update($data, ['user_id' => $id]);
	}
	
	function isUserPassword($name, $password, $user_status = 'A')
	{
		$select = $this->select();
		$select->from('tblUser')
				->columns( ['count' => $this->expression('COUNT(*)')] )
				->where( [
							'user_name' => ucwords(strtolower($name)),
							'password' =>  md5($password),
							'user_status' => $user_status
						] )
		;
		
		//echo $select->getSqlString(); exit();
		$rowset = $this->init('tblUser')->selectWith($select)->current();
		
		return ($rowset->count > 0);
	}
	
	function getId($name, $user_status = 'A')
	{
		$select = $this->select();
		$select->from('tblUser')
				->where( [
							'user_name' => ucwords(strtolower($name)),
							'user_status' => $user_status
						] )
		;
		
		//echo $select->getSqlString(); exit();
		$rowset = $this->init('tblUser')->selectWith($select)->current();
		
		return (!empty($rowset->user_id)) ? $rowset->user_id : NULL;
	}
	
	public function getList($page = NULL, $max_page = 10)
	{
		$result = NULL;
		if (empty($page)) return $this->init('tblUser')->select();
		$select = $this->select();
		$select->from( ['a' => 'tblUser'] )
				->columns( ['*', 'login_name' => $this->expression('a.user_name')] )
				->join( ['b' => 'tblGroup'], 'a.group_id = b.group_id', ['group_name' => 'group_name', 'group_code' => 'group_code'] )
				->join( ['c' => 'tblUser'], 'a.user_create_by = c.user_id', ['create_by' => 'user_name'] )
				->where("a.user_status <> 'D'")
				->order('a.user_name DESC')
		;
		
		//echo $select->getSqlString(); exit();
		$pagination = $this->paginator($select, $page, $max_page);
		foreach ($pagination as $value) {
			$value->password = '';
			$result[] = (array) $value;
		}
		
		return $result;
	}
	
	function getCount()
	{
		$select = $this->select();
		$select->from('tblUser')
				->columns( ['count' => $this->expression('COUNT(*)')] )
				->where("user_status <> 'D'")
		;
		
		// echo $select->getSqlString(); exit();
		$rowset = $this->init('tblUser')->selectWith($select)->current();
		
		return $rowset->count;
	}
	
	public function getRow($id)
	{
		$select = $this->select();
		$select->from('tblUser')
				->where(['user_id' => $id])
		;
		
		// echo $select->getSqlString();
		$rowset = $this->init('tblUser')->selectWith($select)->current();
		if (empty($rowset)) return NULL;
		$rowset->password = '';
		
		return ($rowset->user_status != 'D') ? (array) $rowset : NULL;
	}
	
	function isBlocked($name)
	{
		$select = $this->select();
		$select->from('tblUser')
			->where( ['user_name' => ucwords($name)] )
			->where( [$this->where()->equalTo('user_status', 'I')->OR->equalTo('user_status', 'B')] )
		;
		
		//echo $select->getSqlString();
		$rowset = $this->init('tblDeposit')->selectWith($select)->current();
		$user_name = (!empty($rowset->user_name)) ? $rowset->user_name : NULL;
		if (empty($user_name)) return $user_name;
		
		return strtolower($user_name) == strtolower($name);
	}
	
	function updateLifeTime($id, $time) 
	{
		$data = array();
		$data['user_lifetime'] = $time;
		
		$this->init('tblUser')->update($data, ['user_id' => $id]);
	}
	
	function isOnLogin($id)
	{
		if (empty($id)) return false;
		$select = $this->select();
		$select->from('tblUser')
				->where( ['user_id' => $id] )
		;
		
		//echo $select->getSqlString();
		$rowset = $this->init('tblUser')->selectWith($select)->current();
		
		return ($rowset->user_lifetime > time());
	}
	
	function incPasswordAttempt($id)
	{
		$select = $this->select();
		$select->from('tblUser')
				->where( ['user_id' => $id] );
		//echo $select->getSqlString();
		
		$rowset = $this->init('tblUser')->selectWith($select)->current();
		
		$password_attempt = $rowset->password_attempt;
		$user_status = NULL;
		if (($rowset->password_attempt < 0) && ($rowset->password_attempt >= -10)) {
			$password_attempt--;
		} else if (($rowset->password_attempt >= 0) && ($rowset->password_attempt <= 9)) {
			$password_attempt++;
		} else {
			$password_attempt = 10;
			$user_status = 'B';
		}
		
		$data = array();
		$data['password_attempt'] = $password_attempt;
		if (!empty($user_status)) $data['user_status'] = $user_status;
		$this->init('tblUser')->update($data, ['user_id' => $id]);
		
		return false;
	}
}
