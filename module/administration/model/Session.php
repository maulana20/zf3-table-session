<?php
namespace Administration\Model;

use Application\Model\Table_Gateway_Adapter;

class Session extends Table_Gateway_Adapter
{
	static function get($code)
	{
		$table_gateway = new Table_Gateway_Adapter();
		
		$select = $table_gateway->select();
		$select->from('tblSession')
				->where(['session_code' => $code])
				->order("session_id DESC")
		;
		
		// echo $select->getSqlString(); exit();
		$rowset = $table_gateway->init('tblSession')->selectWith($select)->current();
		if ($rowset) {
			$data = unserialize($rowset->session_data);
			foreach($data->__key_expired__ as $key => $expr) {
				if ($expr < gmdate("Y-m-d H:i:s", time())){
					unset($data->$key);
					unset($data->__key_expired__[$key]);
				}
			}
		}
		
		return $data;
	}
	
	static function save($session)
	{
		$table_gateway = new Table_Gateway_Adapter();
		
		$data = array();
		$data['session_data'] = serialize($session);
		$data['user_id'] = ($session->user_id) ? (int) $session->user_id : 0;
		
		return $table_gateway->init('tblSession')->update($data, ['session_code' => $session->__getCode()]);
	}
	
	static function create()
	{
		$table_gateway = new Table_Gateway_Adapter();
		
		$data = [];
		$data['session_code'] = uniqid(rand()) . uniqid();
		
		$table_gateway->init('tblSession')->insert($data);
		
		return (new SessionInterface($data['session_code']));
	}
	
	static function delete($session)
	{
		$table_gateway = new Table_Gateway_Adapter();
		return $table_gateway->init('tblSession')->delete(['session_code', $session->__getCode()]);
	}
	
	static function deleteByCode($code)
	{
		$table_gateway = new Table_Gateway_Adapter();
		return $table_gateway->init('tblSession')->delete(['session_code', $code]);
	}
	
	static function deleteByUserID($id)
	{
		$table_gateway = new Table_Gateway_Adapter();
		return $table_gateway->init('tblSession')->delete(['user_id', $id]);
	}
	
	static function getByUser($user_id)
	{
		$table_gateway = new Table_Gateway_Adapter();
		
		$result = null;
		
		$select = $table_gateway->select();
		$select->from('tblSession')
				->where('user_id', $user_id)
				->order("session_id DESC")
		;
		
		$list = $table_gateway->init('tblSession')->selectWith($select);
		foreach ($list as $data) {
			array_push($result, (new SessionInterface($data->session_code, $data->session_data)));
		}
		
		return $result;
	}
}

class SessionInterface
{
	private $__code__ = '';
	private $__lock__ = false;
	public $__key_expired__ = array();
	public $cookie_name = 'TESTSESSION';
	
	public function __construct($session_code)
	{
		$this->__code__ = $session_code;
	}
	
	public function __set($key, $data)
	{
		if ($this->__lock__) return;
		$this->$key = $data;
	}

	public function __del($key)
	{
		if ($this->__lock__) return;
		unset($this->$key);
		
		return null;
	}
	
	public function __getJSON()
	{
		return (get_object_vars($this)) ? serialize($this) : "";
	}
	
	public function __getCode()
	{
		return $this->__code__;
	}
	
	public function setExpirationSeconds($sec, $key = null)
	{
		if (!$key) {
			setcookie($this->cookie_name, $this->__getCode(), time() + $sec, "/");
		} else {
			$this->__key_expired__[$key] = gmdate("Y-m-d H:i:s", (time() + $sec));
		}
	}
	
	public function unsetAll()
	{
		if ($this->__lock) return;
		
		foreach(get_object_vars($this) as $key=>$val) {
			if ($key != '__code__' && $key != '__lock__') unset($this->$key);
		}
	}
	
	public function forget()
	{
		unset($_COOKIE[$this->cookie_name]);
		setcookie($this->cookie_name, '', time() - 3600);
	}
	
	public function lock()
	{
		$this->__lock__ = true;
	}
	
	public function isLocked()
	{
		return $this->__lock__;
	}
	
	public function unLock()
	{
		$this->__lock__ = false;
	}
}
