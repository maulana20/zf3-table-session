<?php
namespace Administration\Model;

use Application\Model\Table_Gateway_Adapter;

class Group extends Table_Gateway_Adapter
{
	function getAccess($group_id)
	{
		$select = $this->select();
		$select->from('tblGroup')
				->where( ['group_id' => $group_id] )
		;
		
		// echo $select->getSqlString(); exit();
		$rowset = $this->init('tblGroup')->selectWith($select)->current();
		
		return $rowset->group_access;
	}
	
	function getAccessAll()
	{
		$result = array();
		
		$rowset = $this->init('tblProfile')->select();
		foreach ($rowset as $value) $result[] = $value->profile_code;
		
		return $result;
	}
}
