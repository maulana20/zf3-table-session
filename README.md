# zf3-table-session

## Getting Started

- custom modul table gateway adapter pada pemanggilan semua table dengan customize zend-db.
- pagination hasil data dengan zend-paginator.
- custom session dengan zend-session.
- access control dengan zend-acl.

untuk table session
- generate cookie sesuai aplikasi yang di gunakan.
- simpan cookie ke dalam tabel session berdasarkan user dan cookie yang di gunakan.

running local :
```bash
php -S 0.0.0.0:8080 -t public public/index.php
```
jalankan perintah :
```bash
composer dump-autoload
composer require zendframework/zend-db
composer require zendframework/zend-session
require zendframework/zend-paginator
require zendframework/zend-permissions-acl
```

Tampilan

![login](https://gitlab.com/maulana20/zf3-table-session/-/blob/master/image/login.PNG)

![user](https://gitlab.com/maulana20/zf3-table-session/-/blob/master/image/user.PNG)
